﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
    public GameObject points;
    public Points scriptPoints;
    bool down = false;
	// Use this for initialization
	void Start () {
        points = GameObject.FindGameObjectWithTag("Points");
        scriptPoints = points.GetComponent<Points>();
    }
	
	// Update is called once per frame
	void Update () {

        if (scriptPoints.targetsDown == 3)
        {
            Debug.Log("Cleared!");
            transform.position = new Vector3(transform.position.x, transform.position.y + 2.7f, transform.position.z);

            GetComponent<Transform>().GetChild(0).GetComponent<Light>().enabled = false;

            GetComponent<Transform>().GetChild(0).GetComponent<Light>().transform.position = new Vector3(transform.position.x, transform.position.y - 3.5f, transform.position.z);

            scriptPoints.targetsDown = 0;
            Debug.Log(scriptPoints.targetsDown);

            scriptPoints.puntuation = scriptPoints.puntuation * 3;

            down = false;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(down == false)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 2.7f, transform.position.z);

            GetComponent<Transform>().GetChild(0).GetComponent<Light>().enabled = true;

            GetComponent<Transform>().GetChild(0).GetComponent<Light>().transform.position = new Vector3(transform.position.x, transform.position.y + 3.5f, transform.position.z);

            down = true;
            
            scriptPoints.targetsDown++;
            Debug.Log(scriptPoints.targetsDown);
            scriptPoints.puntuation += 50;
        }

    }
}

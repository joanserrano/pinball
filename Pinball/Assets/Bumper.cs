﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {
    public float bumperForce = 2.5f;
    public GameObject points;
    public Points scriptPoints;

    void Start()
    {
        points = GameObject.FindGameObjectWithTag("Points");
        scriptPoints = points.GetComponent<Points>();
    }

    public void OnCollisionEnter(Collision collision)
    {

        foreach (ContactPoint contact in collision.contacts)
        {
            contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * contact.normal * bumperForce, ForceMode.Impulse);
            scriptPoints.puntuation += 10;
        }
    }
}

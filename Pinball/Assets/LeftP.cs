﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftP : MonoBehaviour
{
    JointSpring spring;
    HingeJoint hingeJoint;
    [SerializeField] float restPosition = 0F;
    [SerializeField] float pressedPosition = 45F;
    [SerializeField] float flipperStrength = 1000F;
    [SerializeField] float flipperDamper = 100F;
    [SerializeField] float direction;

    float turn;

    // Use this for initialization
    void Start()
    {
        hingeJoint = GetComponent<HingeJoint>();


        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;
        hingeJoint.spring = spring;
        hingeJoint.useSpring = true;

    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spring.targetPosition = pressedPosition;
            hingeJoint.spring = spring;
        }
        else
        {
            spring.targetPosition = restPosition;
            hingeJoint.spring = spring;
        }
    }
}

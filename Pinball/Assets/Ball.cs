﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public GameObject Death;
    public GameObject points;
    public Points scriptPoints;

    public bool tuch = false;
    // Use this for initialization
    void Start () {
        Death = GameObject.FindGameObjectWithTag("Respawn");
        points = GameObject.FindGameObjectWithTag("Points");
        scriptPoints = points.GetComponent<Points>();
    }
	
	// Update is called once per frame
	void Update () {
		if (tuch == true)
        {
            if (scriptPoints.balls > 1)
            {
                transform.position = new Vector3(9, 2, -5);
                scriptPoints.balls--;
                tuch = false;
            }
        }
	}

    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject == GameObject.FindGameObjectWithTag("Respawn") && tuch == false)
        {
            if (scriptPoints.balls > 1)
            {
                transform.position = new Vector3(9, 2, -5);
                scriptPoints.balls--;
            }else if(scriptPoints.balls <= 1)
            {
                scriptPoints.gameOver = true;
                tuch = true;
            }
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour {

    public int targetsDown = 0;

    public int puntuation = 0;
    TextMesh scoreLives;

    public int balls;

    public bool gameOver = true;

    // Use this for initialization
    void Start () {
        targetsDown = 0;
        balls = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if(gameOver == false)
        {
            GetComponent<TextMesh>().text = "Points: " + puntuation.ToString() + "\n Balls: " + balls.ToString();
        }
        else if(gameOver == true)
        {
            GetComponent<TextMesh>().text = "Game Over.\n Press <enter> to start";
        }

        if (Input.GetKey(KeyCode.Return))
        {
            if (balls == 0)
            {
                balls = 4;
                gameOver = false;
            }
        }

    }

}
